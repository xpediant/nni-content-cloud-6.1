package com.nni.aem.contentcloud.servlet;

import java.io.IOException;
import java.rmi.ServerException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.email.EmailService;
import com.adobe.acs.commons.email.EmailServiceConstants;
import com.day.cq.wcm.offline.HtmlUtil;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;

import javax.jcr.Node;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;


@SlingServlet(
        resourceTypes = {"sling/servlet/default"},
        methods = {"POST"},
        selectors = {"embeddedcloudemail"}
)
@Properties({
        @Property(name="service.pid", value="com.nni.aem.contentcloud.servlet.HandleSendEmail",propertyPrivate=false),
        @Property(name="service.description",value="Handles requests to send share content emails.", propertyPrivate=false)
})
public class HandleSendEmail extends org.apache.sling.api.servlets.SlingAllMethodsServlet {
	private static final long	serialVersionUID = 1;
	private static final Logger	LOGGER			 = LoggerFactory.getLogger(HandleSendEmail.class);

	@Reference
	private EmailService		emailService;

	@Reference
	private SlingRepository		repository;

	public void bindRepository(SlingRepository repository) {
		this.repository = repository;
	}

	@Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServerException, IOException {
        handleRequest(request,response);
    }

    @Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {

        handleRequest(request,response);
    }


	protected void handleRequest(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {
		try {
			// Encode the submitted form data to JSON
			JSONObject obj = new JSONObject();

			try {
				// Get the submitted form data
				String yourName = request.getParameter("your-name");
				String yourEmail = request.getParameter("your-email");
				
				if (StringUtils.isBlank(yourName) || StringUtils.isBlank(yourEmail)) {
					Principal userPrincipal = request.getUserPrincipal();
					
					if (userPrincipal != null) {
						final ResourceResolver resourceResolver = request.getResourceResolver();
						UserManager userManager = resourceResolver.adaptTo(UserManager.class);
						Authorizable authorizable = userManager.getAuthorizable(userPrincipal);
						Resource resource = resourceResolver.getResource(authorizable.getPath());
						Node node = resource.adaptTo(Node.class);
						
						if (StringUtils.isBlank(yourName)) {
							javax.jcr.Property familyName = node.getProperty("profile/familyName");
							javax.jcr.Property givenName = node.getProperty("profile/givenName");
							String fName = familyName == null ? "" : familyName.getString();
							String gName = givenName == null ? "" : givenName.getString();
							yourName = (gName + " " + fName).trim();
						}
						
						if (StringUtils.isBlank(yourEmail)) {
							javax.jcr.Property email = node.getProperty("profile/email");

							if (email != null) {
								yourEmail = email.getString();
							} else {
								throw new IllegalArgumentException("Your email address is required.");
							}
						}
					}
				}
				
				String recipientName = request.getParameter("recipient-name");
				String recipientEmail = request.getParameter("recipient-email");
				
				if (StringUtils.isBlank(recipientEmail)) {
					throw new IllegalArgumentException("The recipient's email address is required.");
				}
				
				String subject = request.getParameter("subject").trim();
				String message = request.getParameter("message").trim();
				
				String textToEmbed = request.getParameter("text-to-embed");
				
				String templatePath = request.getResource().getValueMap().get("templatePath").toString();

				Map<String, String> emailParameters = new HashMap<>();
				emailParameters.put(EmailServiceConstants.SENDER_EMAIL_ADDRESS, yourEmail);
				emailParameters.put(EmailServiceConstants.SENDER_NAME, yourName);
				emailParameters.put("recipientName", recipientName);
				emailParameters.put("recipientEmail", recipientEmail);
				emailParameters.put("subject", subject);
				emailParameters.put("message", message);
				emailParameters.put("textToEmbed", HtmlUtil.escapeHtmlText(textToEmbed));

				String[] recipients = new String[] {recipientEmail};

				List<String> failureList = emailService.sendEmail(templatePath, emailParameters, recipients);

				if (failureList.size() == 0) {
					obj.put("status", "success");
				} else {
					obj.put("status", "error");
					obj.put("message", "Unable to email " + recipientEmail + ".  Please check that address.");
				}
			} catch (IllegalArgumentException e) {
				obj.put("status", "error");
				obj.put("message", e.getMessage());

				LOGGER.error("Failed to send share content email due to an internal error.", e);
			} catch (Exception e) {
				obj.put("status", "error");
				obj.put("message", "Failed to send share content email due to an internal error.<br>" + e.getMessage());

				LOGGER.error("Failed to send share content email due to an internal error.", e);
			}

			// Get the JSON formatted data
			String jsonData = obj.toString();

			// Return the JSON formatted data
			response.getWriter().write(jsonData);
		} catch (JSONException e) {
			LOGGER.error("Failed to generate response to share content email request", e);
		}
	}
}
