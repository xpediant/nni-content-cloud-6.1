package com.nni.aem.contentcloud.tags;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Resolve resource using both a path and inheritable property.
 */
public final class ResolveWithInheritedDefaultTag extends TagSupport {
	/** Required. */
	private static final long serialVersionUID = 1L;

	/** Resource path. */
	private String			  path;

	/** Resource property */
	private String			  property;

	/** variable name */
	private String			  var;

	private boolean resolvedUsing(String path) {
		if (StringUtils.isNotBlank(path)) {
			SlingHttpServletRequest request = (SlingHttpServletRequest) pageContext.getRequest();
			Resource resource = request.getResourceResolver().getResource(request.getResource(), path);

			if (resource != null && !resource.getResourceType().equals(Resource.RESOURCE_TYPE_NON_EXISTING)) {
				pageContext.setAttribute(getVar(), resource);
				return true;
			}
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
	 */
	public int doEndTag() throws JspTagException {
		SlingHttpServletRequest request = (SlingHttpServletRequest) pageContext.getRequest();

		if (!resolvedUsing(getPath())) {
			if (StringUtils.isNotBlank(getProperty())) {
				InheritanceValueMap inheritanceValueMap = new HierarchyNodeInheritanceValueMap(request.getResource());
				String defaultPath = inheritanceValueMap.getInherited(getProperty(), String.class);

				resolvedUsing(defaultPath);
			}
		}

		return EVAL_PAGE;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}
}
