package com.nni.aem.contentcloud.tags;

import com.day.cq.wcm.api.Page;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * Decodes the provided resource request into an array of resource paths specifying the embedded content. 
 */
public final class EmbeddedResourcesTag extends TagSupport implements EmbeddedResourcesEncyption {
	/** Required. */
	private static final long serialVersionUID = 1L;

	/** Required variable name to hold the decoded resource path array */
	private String			  var;

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
	 */
	public int doEndTag() throws JspTagException {
		try {
			SlingHttpServletRequest request = (SlingHttpServletRequest) pageContext.getRequest();
            Page currentPage = (Page) pageContext.getAttribute("currentPage");
			RequestPathInfo requestPathInfo = request.getRequestPathInfo();
			String suffix = requestPathInfo.getSuffix();

			String[] resourcePaths;

			if (suffix == null || currentPage == null) {
				resourcePaths = new String[0];
			}
            else {
				resourcePaths = suffix.substring(1).split("/");

                    //to shorten url we use the page path as base, and the rest as paths relative to the page path.
                    final String pagebaseurlDecryptedPath = currentPage.getPath();
                    List<String> decryptedPaths = new ArrayList<>();

                    for (int i = 0; i < resourcePaths.length; i++) {
                        final String decryptedPath =DES.decrypt(resourcePaths[i]);
                        if (StringUtils.isNotBlank(decryptedPath)) {
                            decryptedPaths.add(pagebaseurlDecryptedPath+decryptedPath);
                        }
                    }
                    resourcePaths = decryptedPaths.toArray(new String[0]);
			}
			pageContext.setAttribute(getVar(), resourcePaths);
		} catch (GeneralSecurityException | IllegalArgumentException e) {
			throw new JspTagException(e);
		}

		return EVAL_PAGE;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}
}
