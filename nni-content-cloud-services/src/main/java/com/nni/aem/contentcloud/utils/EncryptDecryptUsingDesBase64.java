package com.nni.aem.contentcloud.utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import java.util.Base64;

public class EncryptDecryptUsingDesBase64 {
	private Cipher	 encryptCipher;
	private Cipher	 decryptCipher;

	private static final int ITERATION_COUNT	= 10;

	// 8-byte Salt

	private static byte[]	 SALT			= { (byte) 0xF3, (byte) 0x0E, (byte) 0xD3, (byte) 0xB8,
			(byte) 0x74, (byte) 0x11, (byte) 0x63, (byte) 0xD3 };

	public EncryptDecryptUsingDesBase64(String passPhrase) {
		this(passPhrase, SALT);
	}
	
	public EncryptDecryptUsingDesBase64(String passPhrase, byte[] salt) {
		this(passPhrase, salt, ITERATION_COUNT);
	}
	
		public EncryptDecryptUsingDesBase64(String passPhrase, byte[] salt, int iterationCount) {
		try {
			// generate PBEKey using provided password, salt, iteration count
			KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);

			// create a secret (symmetric) key using PBE with MD5 and DES
			SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

			// construct a parameter set for password-based encryption as
			// defined in the PKCS #5 standard

			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

			final String algorithm = key.getAlgorithm();
			encryptCipher = Cipher.getInstance(algorithm);
			decryptCipher = Cipher.getInstance(algorithm);

			// initialize the ciphers with the given key

			encryptCipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			decryptCipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	public String encrypt(String plainText) throws GeneralSecurityException {
		byte[] plainBytes = plainText.getBytes(Charset.defaultCharset());
		byte[] encryptedBytes = encryptCipher.doFinal(plainBytes);
		return Base64.getUrlEncoder().encodeToString(encryptedBytes);
	}

	public String decrypt(String encryptedText) throws GeneralSecurityException {
		byte[] encryptedBytes = Base64.getUrlDecoder().decode(encryptedText);
		byte[] plainBytes = decryptCipher.doFinal(encryptedBytes);
		return new String(plainBytes, Charset.defaultCharset());
	}

}
