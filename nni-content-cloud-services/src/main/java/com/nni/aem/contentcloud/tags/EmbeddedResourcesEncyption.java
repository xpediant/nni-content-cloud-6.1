package com.nni.aem.contentcloud.tags;


import com.nni.aem.contentcloud.utils.EncryptDecryptUsingDesBase64;
import com.nni.aem.contentcloud.utils.SimpleEncoder;

public interface EmbeddedResourcesEncyption {
	//static final EncryptDecryptUsingDesBase64 DES = new EncryptDecryptUsingDesBase64("ResourceSelector");
    static final SimpleEncoder DES = new SimpleEncoder();
}
