package com.nni.aem.contentcloud.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.jcr.AccessDeniedException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRXUtils {
    private static final Logger log          = LoggerFactory.getLogger(CRXUtils.class);

    public static String getStringValueOfProperty(Node node, String propertyName, String defaultValue) {
        String nodePath = "";
        try {
            if (node == null || propertyName == null) {

                log.error("getStringProperty: node or propertyName is null");
                return defaultValue;
            }

            nodePath = node.getPath();

           Property property = null;
           try {
               property = node.getProperty(propertyName);
           } catch (PathNotFoundException e){
               log.warn("PathNotFoundException for nodePath:"+propertyName);
           }

           if (property == null) {
                return null;
           }

            return property.getString();

        }
        catch (RepositoryException e) {
            log.error("Node path is : " + nodePath);
            log.error("getStringValueOfProperty1: ", e);
        }
        return defaultValue;
    }

    public static List<String> getStringValuesOfProperty(Node node, String propertyName) {
        List<String> values = new ArrayList<String>();
        try {
            if (node == null || propertyName == null) {

                log.error("getStringValuesOfProperty: node or propertyName is null");
                return null;
            }

            Property prop = node.getProperty(propertyName);
            if (prop.isMultiple()) {
                Value[] propValues = node.getProperty(propertyName).getValues();
                for (Value propValue : propValues) {
                    values.add(propValue.getString());
                }

            }
            else {
                values.add(prop.getString());
            }
        }
        catch (RepositoryException rex) {
            log.error("Error trying to get String values of property : " + propertyName);
        }
        return values;
    }

    public static String getDelimittedStringForNodeProperty(Node node, String propertyName, String delimitter) {
        if (null == node || null == propertyName || propertyName.trim().isEmpty() || null == delimitter || delimitter.trim().isEmpty()) {
            return "";
        }
        List<String> values = getStringValuesOfProperty(node, propertyName);
        StringBuffer delimittedString = new StringBuffer();
        int countValues = 0;
        for (String value : values) {
            delimittedString.append(countValues++ > 0 ? delimitter : "");
            delimittedString.append(value);
        }
        return delimittedString.toString();
    }

    public static String getPathOfNode(Node node) {
        if (null != node) {
            try {
                return node.getPath();
            }
            catch (RepositoryException e) {
                log.error("getPathOfNode: ", e);
            }
        }

        return null;
    }

    public static Node getNodeFromPath(String path, Session session) {
        try {
            if (path == null || path.length() < 1) {
                log.error("getNodeFromPath: path is null / empty");
                return null;
            }
            log.debug("getNodeFromPath: Getting node for path : " + path);

            return (Node) session.getItem(path);
        }
        catch (PathNotFoundException e) {
            log.error("Error in getNodeFromPath: ", e);
        }
        catch (RepositoryException e) {
            log.error("Error in getNodeFromPath: ", e);
        }
        return null;
    }

    public static Node getParentNode(Node childNode) {
        try {
            if (childNode == null) {
                log.error("getParentNode: id childNode null");
                return null;
            }
            return childNode.getParent();
        }
        catch (AccessDeniedException e) {
            log.error("getParentNode: ", e);
        }
        catch (ItemNotFoundException e) {
            log.error("getParentNode: ", e);
        }
        catch (RepositoryException e) {
            log.error("getParentNode: ", e);
        }
        return null;
    }

    public static Calendar getNodeDateProperty(Node node, String propertyName) {
        try {
            if (node == null || propertyName == null || propertyName.length() < 1) {
                log.error("getNodeDateProperty: one of node, propertyName or propertyValue was null / empty");
                return null;
            }
            return node.getProperty(propertyName).getDate();
        }
        catch (ValueFormatException e) {
            log.error("getNodeDateProperty: ", e);
        }
        catch (PathNotFoundException e) {
            log.error("getNodeDateProperty: ", e);
        }
        catch (RepositoryException e) {
            log.error("getNodeDateProperty: ", e);
        }
        return null;
    }

    public static String getNodeStringProperty(Node node, String propertyName) {
        String nodePath = null;
        try {
            nodePath = node.getPath();
            if (node == null || propertyName == null || propertyName.length() < 1) {
                log.error("getNodeStringProperty: one of node, propertyName or propertyValue was null / empty");
                return null;
            }
            return node.getProperty(propertyName).getString();
        }
        catch (RepositoryException e) {
            String message = String.format("Unable to retrieve property. Node: %s. Property: %s", nodePath, propertyName);
            log.error(message, e);
        }
        return null;
    }

    public static List<String> getStringPropertyAsList(Node node, String property) {
        List<String> list = new ArrayList<String>();
        String nodePath = null;
        try {
            nodePath = node.getPath();
            if (node.hasProperty(property)) {
                Property prop = node.getProperty(property);
                if (prop.isMultiple()) {
                    Value[] propValues = prop.getValues();
                    for (Value value : propValues) {
                        String strValue = value.getString();
                        list.add(strValue);
                    }
                }
                else {
                    String strValue = prop.getValue().getString();
                    list.add(strValue);
                }
            }
        }
        catch (PathNotFoundException e) {
            log.error("PathNotFoundException in getStringPropertyAsList. Message: " + e.getMessage());
            log.error("PATH:" + nodePath + " Property:" + property);
            list.clear();
            return list;
        }
        catch (RepositoryException e) {
            log.error("RepositoryException in getStringPropertyAsList. Message: " + e.getMessage());
            log.error("PATH:" + nodePath + " Property:" + property);
            list.clear();
            return list;
        }
        return list;
    }

    public static List<String> getFormattedPaths(List<String> paths, ResourceResolver resResolver) {
        ArrayList<String> formattedPaths = new ArrayList<String>();

        if (formattedPaths != null) {
            for (String path : paths) {
                Resource res = resResolver.getResource(path);
                
                String resType = res == null ? null : res.getResourceType();
                if (resType == null) {
                	formattedPaths.add(String.format("<del>%s</del>", path));
                } else if ("cq:Page".equals(resType)) {
                    formattedPaths.add(String.format("<a href='%s.html' target=_blank>%s</a>", path.replace(" ", "%20"), path));
                } else if ("dam:Asset".equals(resType)) {
                    formattedPaths.add(String.format("<a href='/damadmin#%s' target=_blank>%s</a>", path.replace(" ", "%20"), path));
                } else if (path.startsWith("/content/dam/")) {
                    formattedPaths.add(String.format("<a href='/damadmin#%s' target=_blank>%s</a>", path.replace(" ", "%20"), path));
                } else {
                    formattedPaths.add(path);
                }
            }
        }
        return formattedPaths;
    }
    
    public static String getAbsolutePath(String path, ResourceResolver resResolver, String server) {
    	String absolutePath = null;
    	Resource res = resResolver.getResource(path);
        String resType = res.getResourceType();
        //path= externalizer.externalLink(resResolver, Externalizer.PUBLISH, "http", path);
        path = server + path;
        if ("cq:Page".equals(resType)) {
            absolutePath = String.format("%s.html", path.replace(" ", "%20"), path);
        }
        else if ("dam:Asset".equals(resType)) {
        	absolutePath = String.format("/damadmin#%s", path.replace(" ", "%20"), path);
        }
        else if (path.startsWith("/content/dam/")) {
        	absolutePath = String.format("/damadmin#%s", path.replace(" ", "%20"), path);
        }
        else {
        	absolutePath = path;
        }
        return absolutePath;
    }
}
