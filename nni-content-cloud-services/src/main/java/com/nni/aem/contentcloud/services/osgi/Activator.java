package com.nni.aem.contentcloud.services.osgi;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Activator implements BundleActivator {

    private static final Logger LOGGER = LoggerFactory.getLogger(Activator.class);

    @Override
    public void start(final BundleContext bundleContext) {
        LOGGER.info(bundleContext.getBundle().getSymbolicName() + " starting");
        LOGGER.info(bundleContext.getBundle().getSymbolicName() + " started");
    }


    @Override
    public void stop(final BundleContext context) {
        LOGGER.info(context.getBundle().getSymbolicName() + " stopped");
    }
}
