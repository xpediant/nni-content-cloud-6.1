package com.nni.aem.contentcloud.tags;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Resolve resource using both a path and inheritable property.
 */
public final class ResolveWithInheritedDefaultPropertyTag extends TagSupport {
	/** Required. */
	private static final long serialVersionUID = 1L;

	/** Resource property */
	private String			  property;

	/** variable name */
	private String			  var;

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
	 */
	public int doEndTag() throws JspTagException {
		SlingHttpServletRequest request = (SlingHttpServletRequest) pageContext.getRequest();
        Page currentPage = (Page) pageContext.getAttribute("currentPage");

         String propertyName = getProperty();
         String varName = getVar();

        if (StringUtils.isNotEmpty(propertyName) && StringUtils.isNotEmpty(varName) && currentPage!=null) {
           InheritanceValueMap inheritanceValueMap = new HierarchyNodeInheritanceValueMap(currentPage.getContentResource());
           String contentCloudISIPath =  inheritanceValueMap.getInherited(propertyName,String.class);
           pageContext.setAttribute(varName,contentCloudISIPath);
        }

		return EVAL_PAGE;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}
}
