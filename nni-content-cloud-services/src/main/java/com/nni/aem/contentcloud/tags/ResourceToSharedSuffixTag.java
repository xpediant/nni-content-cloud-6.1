package com.nni.aem.contentcloud.tags;


import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.security.GeneralSecurityException;
import com.day.cq.wcm.api.Page;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

/**
 * Creates an embedded content suffix suitable for displaying the specified resource path.
 */
public final class ResourceToSharedSuffixTag extends TagSupport implements EmbeddedResourcesEncyption {
	/** Required. */
    private static final long serialVersionUID = 1L;

    /** Optional variable name */
    private String varFullSuffix;


	private String varComponentSuffix;


    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
     */
    public int doEndTag() throws JspTagException {  
    	try {

			SlingHttpServletRequest request = (SlingHttpServletRequest) pageContext.getRequest();
			Page currentPage = (Page) pageContext.getAttribute("currentPage");
			Resource resource = (Resource) pageContext.getAttribute("resource");

			if ((resource!=null) && (currentPage != null) && (request !=null)){
				//Full URL
                if (StringUtils.isNotBlank(getVarFullSuffix())){
				 String resourcePath=resource.getPath();
				 String suffix = DES.encrypt(resourcePath);
				 pageContext.setAttribute(getVarFullSuffix(), suffix);
                }

				//Get Relative URL
                String currentPagePath = currentPage.getPath();
                String resourcePath=resource.getPath();
				String componentPath = resourcePath.replace(currentPagePath,"");
				String component = DES.encrypt(componentPath);
				pageContext.setAttribute(getVarComponentSuffix(), component);
			}

		} catch (Exception e) {
			throw new JspTagException(e);
		}
    	
        return EVAL_PAGE;
    }

	public String getVarFullSuffix() {
		return varFullSuffix;
	}

	public void setVarFullSuffix(String varFullSuffix) {
		this.varFullSuffix = varFullSuffix;
	}

	public String getVarComponentSuffix() {
		return varComponentSuffix;
	}

	public void setVarComponentSuffix(String varComponentSuffix) {
		this.varComponentSuffix = varComponentSuffix;
	}
}
