package com.nni.aem.contentcloud.utils;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.Base64;

public class SimpleEncoder {

    public String encrypt(String plainText) throws GeneralSecurityException {

       String encodedString =  plainText.toLowerCase()
                 .replace("/jcr:content/","{0}")
                 .replace("/","{1}")
                 .replace(".","{2}");

       return encodedString;
    }

    public String decrypt(String encryptedText) throws GeneralSecurityException {
        String decodedString =  encryptedText
                .replace("{0}","/jcr:content/")
                .replace("{1}","/")
                .replace("{2}",".");

        return decodedString;
    }


}
