<%@ page trimDirectiveWhitespaces="true" %>
<%@include file="/apps/contentcloud/includes/global.jsp"%>

<scc:ResourceToSharedSuffix varComponentSuffix="embeddedComponentSuffix"/>

<div class="shareableContainer"  data-cloud-embedded="${embeddedComponentSuffix}" data-cloud-embedded-tag="${properties.embeddedContentTag}" data-cloud-embedded-selected="1">
	<cq:include path="embedded" resourceType="foundation/components/parsys" />
</div>