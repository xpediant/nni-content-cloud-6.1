<%@ page import="java.util.HashMap" %>
<%@include file="/apps/contentcloud/includes/global.jsp"%>
<%@page session="false" contentType="text/html; charset=utf-8"%>

<cq:include path="embedded" resourceType="foundation/components/parsys" />

<%-- Disclaimer --%>
<c:if test="${empty properties.overrideDisclaimer}">
    <scc:resolveWithInheritedDefaultProperty var="contentCloudDisclaimer" property="contentCloudDisclaimer" />
</c:if>
<c:if test="${not empty properties.overrideDisclaimer}">
    <c:set var='contentCloudDisclaimer' value="${properties.pageDisclaimer}"/>
</c:if>

<%--SubText --%>
<c:if test="${empty properties.overridePageParentText}">
    <scc:resolveWithInheritedDefaultProperty var="contentCloudPageParentText" property="contentCloudPageParentText" />
</c:if>
<c:if test="${not empty properties.overridePageParentText}">
    <c:set var='contentCloudPageParentText' value="${properties.pageParentText}"/>
</c:if>


<%-- Prescring information --%>
<c:if test="${empty properties.overridePrescribingInformation}">
    <scc:resolveWithInheritedDefaultProperty var="contentCloudPrescribingInformation" property="contentCloudPrescribingInformation" />
</c:if>

<c:if test="${not empty properties.overridePrescribingInformation}">
    <c:set var='contentCloudPrescribingInformation' value="${properties.prescribingInformation}"/>
</c:if>

<%-- ISI--%>

<c:set var="embeddedcloudProperties" value="<%= new HashMap<String,String>(8)%>" scope="request"/>

<%-- Custom Properties --%>
<c:set var="customProperties" value="${widgets:getMultiFieldPanelValues(resource, 'customProperties')}"/>
<c:forEach items="${customProperties}" var="customProperty">
    <c:set target="${embeddedcloudProperties}" property="${customProperty['name']}" value="${customProperty['value']}"/>
</c:forEach>

<%-- Context Variables
disclaimer,pageParentText,prescribingInformation,isiOptions <!-- <c:out value="${map['key']}"/> -->
--%>
<c:set target="${embeddedcloudProperties}" property="embeddedContentName" value="${properties.embeddedContentName}"/>
<c:set target="${embeddedcloudProperties}" property="disclaimer" value="${contentCloudDisclaimer}"/>
<c:set target="${embeddedcloudProperties}" property="pageParentText" value="${contentCloudPageParentText}"/>
<c:set target="${embeddedcloudProperties}" property="prescribingInformation" value="${contentCloudPrescribingInformation}"/>
<c:set target="${embeddedcloudProperties}" property="isiOptions" value="${properties.isiOptions}"/>








