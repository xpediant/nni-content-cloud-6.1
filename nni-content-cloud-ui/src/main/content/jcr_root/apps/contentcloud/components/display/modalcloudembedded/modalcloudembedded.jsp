<%@ page trimDirectiveWhitespaces="true" %>
<%@include file="/apps/contentcloud/includes/global.jsp"%>


<c:set var='uuid' value="<%=java.util.UUID.randomUUID().toString() %>"/>

<div class="tabbed-modal-contentcloud">
    <div class="cloudembeddedModalDialog ${uuid}" title="Share Content" style="display:none">
        <form action="${resource.path}.embeddedcloudemail.html" method="post">
            <ul class="tabs">
                <li><a href="#cloudembeddedModalDialog1${uuid}">${properties.shareNowLinkTitle}</a></li>
                <li><a href="#cloudembeddedModalDialog2${uuid}">${properties.embedLinkTitle}</a></li>
            </ul>
            <div id="cloudembeddedModalDialog1${uuid}">
                <c:if test="${not empty properties.shareNowTitle}"><h4>${properties.shareNowTitle}</h4></c:if>
                ${properties.shareNowHeader}
                <fieldset>
                    <fieldset>
                        <label for="your-name">${properties.shareNowNameLabel}</label> <input
                            type="text" value="" class="shareable-sender-name-field validate"
                            name="your-name" />
                    </fieldset>
                    <fieldset>
                        <label for="your-email">${properties.shareNowEmailLabel}</label> <input
                            type="email" class="shareable-your-email-field required email"
                            name="your-email">
                    </fieldset>
                </fieldset>
                <fieldset>
                    <fieldset>
                        <label for="recipient-name">${properties.shareNowRecipientNameLabel}</label>
                        <input type="text" value=""
                               class="shareable-recipient-name-field validate" name="recipient-name">
                    </fieldset>
                    <fieldset>
                        <label for="recipient-email">${properties.shareNowRecipientEmailLabel}</label>
                        <input type="email"
                               class="shareable-recipient-email-field required email"
                               name="recipient-email">
                    </fieldset>
                </fieldset>
                <fieldset>
                    <fieldset>
                        <label for="subject">${properties.shareNowSubjectLabel}</label>
                        <input type="text" value=""
                               class="shareable-subject-field validate" name="subject">
                    </fieldset>
                    <fieldset>
                        <label for="message">${properties.shareNowMessageLabel}</label>
                        <textarea class="shareable-message-field" name="message">
                        </textarea>
                    </fieldset>
                </fieldset>
                <div class="clear-floats"></div>
                <div class="button-group">
                    <p class="button button-brand-color button-shareable-send">
                        <input type="submit" value="${properties.shareNowSendLabel}">
                    </p>
                    <p class="button text-only button-reset">
                        <input type="reset" value="${properties.shareNowResetLabel}">
                    </p>
                </div>
                <div class="results" style="display:none">
                </div>
            </div>
            <div id="cloudembeddedModalDialog2${uuid}">
                <c:if test="${not empty properties.embedTitle}"><h4>${properties.embedTitle}</h4></c:if>
                ${properties.embedHeader}

				<textarea class="text-to-embed-field hidden"><%--
				 The content of this textarea is indented separately from the rest of the page was its indentation will be part of the generated email message.
				 --%>
                  <div class="iframe-container">
	                 <iframe
	                     class="iframe-content-cloud-content"
	                     src="$CONTENT_SERVERFULLPATH.embeddedcloud.background:white.html/$CONTENT_URL.html"
	                     allowTransparency="true" scrolling="no" frameborder="0"
	                     style="border: none; overflow: hidden;">
	                  </iframe>
                  </div>

                  <script src="$CONTENT_SERVER/etc/designs/contentcloud/clientlibs/iframeexternal.js"></script>
				</textarea>
               <textarea class="text-to-embed-field" readonly="readonly" name="text-to-embed">

               </textarea>
            </div>
            <%-- analytics event--%>
            <a class="analytics hidden" data-cloud-embedded-tag="${properties.shareablecontentTag}"  data-analytics-category='Embed Frames'  data-analytics-action='Instructions Emailed' data-analytics-label='${properties.shareablecontentName}' target="_blank" >Share</a>
        </form>
    </div>

    &nbsp;
    <a  class="cloudembeddedControl button" data-uuid="${uuid}"  data-cloud-embedded-tag="${properties.shareablecontentTag}"  style="right: 185px"        data-analytics-category='Embed Frames'  data-analytics-action='Instructions Displayed' data-analytics-label='${properties.shareablecontentName}' target="_blank" >Share</a>
    <a  class="cloudembeddedControl share-now button" data-uuid="${uuid}" data-cloud-embedded-tag="${properties.shareablecontentTag}" style="right: 75px" data-analytics-category='Embed Frames'  data-analytics-action='Instructions Displayed' data-analytics-label='${properties.shareablecontentName}' target="_blank" >Share Now</a>
    <a  class="cloudembeddedControl share-embed button"  data-uuid="${uuid}" data-cloud-embedded-tag="${properties.shareablecontentTag}">Embed</a>
</div>
