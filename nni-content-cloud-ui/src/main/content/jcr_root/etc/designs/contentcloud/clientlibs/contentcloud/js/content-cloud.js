$(function() {

	var cloudembeddedControls = $('.cloudembeddedControl');

	if (cloudembeddedControls) {

        cloudembeddedControls.on('click', function(e) {

		e.preventDefault();

		var modalDialogid = $(this).data('uuid');

		var modal = $(".cloudembeddedModalDialog."+modalDialogid);
		var button = $(this);
		var embedOnly = button.hasClass('share-embed');
		var shareNowOnly = button.hasClass('share-now');
        var buttonTag =  $(this).data('cloud-embedded-tag');

        //Server URL
        var serverUrlInformation = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
        var fullPathServerUrlInformation = window.location.pathname;
        fullPathServerUrlInformation = serverUrlInformation+fullPathServerUrlInformation.replace(/.html$/,"");

		if (embedOnly == shareNowOnly) {
			modal.tabs('enable').tabs('option', 'active', 0);
			$(".tabs", modal).show();
		} else if (embedOnly) {
			modal.tabs('enable').tabs('disable', 0).tabs('option', 'active', 1);
			$(".tabs", modal).hide();
		} else if (shareNowOnly) {
			modal.tabs('enable').tabs('disable', 1).tabs('option', 'active', 0);
			$(".tabs", modal).hide();
		}

        //get the shareable containers/components that match tag
         var selectedContainersComponents =   $("[data-cloud-embedded][data-cloud-embedded-tag='"+buttonTag+"'][data-cloud-embedded-selected='1']")
             .map(function() {
              return $(this).data('cloud-embedded');
         }).get().join('/');

        if (selectedContainersComponents == undefined || selectedContainersComponents == "") {
            return;
        }

		var ta = $('textarea.text-to-embed-field.hidden', modal).each(function( index ) {
            var v =	$(this).html();
            v = v.replace(/\$CONTENT_URL/g,selectedContainersComponents);
            v = v.replace(/\$CONTENT_SERVERFULLPATH/g,fullPathServerUrlInformation);
            v = v.replace(/\$CONTENT_SERVER/g,serverUrlInformation);
            $(this).next('textarea').html(v);
        });

		$(".cloudembeddedModalDialog."+modalDialogid).dialog('open');
	});

      $(".cloudembeddedModalDialog").tabs().dialog({autoOpen: false, modal: true});
	}
});


$(function() {
    var form = $(".cloudembeddedModalDialog form");
    
    if (document.queryCommandSupported('copy')) {
    	var textarea = $(".text-to-embed-field", form);
    	textarea.parent().append('<div><button type="button" class="ccloud-copy-to-clipboard">Copy to Clipboard</button></div>');
    	
    	form.find('.ccloud-copy-to-clipboard').on('click', function() {
    		textarea.select();
    		document.execCommand('copy');
    	});
    }

    form.on('submit', function(event) {
        event.preventDefault();
        thisform = this;

        $("div.results", thisform).hide().empty();
        $(".analytics.hidden", thisform).click();

        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data){
                var results = $("div.results", $(thisform));
                if (data.status == 'success') {
                    results.css('border', '1px solid green');
                    results.text("Message Sent");
                } else {
                    results.css('border', '1px solid red');
                    results.append(data.message);
                }
                results.show();
            },
            error: function(jqXHR, status) {
                var results = $("div.results", $(thisform));
                results.css('border', '1px solid red').append("The request failed.<br>Please try again later.<br>").append(status).show();
            }
        });
    });
});



var ccloud = {
    toggleSelection: function(element) {
        var closestElement = $(element).closest('[data-cloud-embedded]');
        if (closestElement.attr("data-cloud-embedded-selected")) {
            closestElement.removeAttr('data-cloud-embedded-selected');
        } else {
            closestElement.attr('data-cloud-embedded-selected','1');
        }
    }
};


